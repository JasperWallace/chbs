extern crate rand;

use std::env;
use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;

use rand::Rng;

// Read dictonary file [done]
// get 4 random words [done]
// print them [done]
//

// /home/jasper/sysadmin/sources/build-linux/xkpasswd/xkpasswd-v0.2.1

// /usr/share/dict/british-english

// if command line arg use that word list [done]
// ignore words starting with a capital [done]
// check for ascii only
// wrap the wordlist in an iterator that returns a word at a random position. [wow!?!]
// https://doc.rust-lang.org/std/iter/trait.Iterator.html
//

/// An iterator that returns a random item
struct RandIter {
    thing: Vec<String>,
}

/// the impl
impl RandIter {
    fn new(avec: Vec<String>) -> RandIter {
        RandIter { thing: avec }
    }
}

impl Iterator for RandIter {
    type Item = String;

    // next() is the only required method
    fn next(&mut self) -> Option<String> {
        if !self.thing.is_empty() {
            let idx = rand::thread_rng().gen_range(0, self.thing.len());
            let ref res: String = self.thing[idx];
            Some(res.to_string())
        } else {
            None
        }
    }
}

fn main() {
    let mut argv = env::args();
    let mut wordfile: String = "/usr/share/dict/words".to_string();
    match argv.nth(1) {
        Some(arg) => wordfile = arg,
        None => {}
    };

    println!("Using {} as the list of words", wordfile);

    let f = match File::open(wordfile) {
        Ok(f) => f,
        Err(err) => {
            println!("Error: {}", err);
            return;
        }
    };

    let reader = BufReader::new(f);

    let mut wordlist: Vec<String> = Vec::new();

    for line in reader.lines() {
        let word = match line {
            Ok(word) => word,
            Err(err) => {
                println!("Error: {}", err);
                break;
            }
        };
        // no annoying apostrophies
        if word.contains('\'') {
            continue;
        }
        // and no small word
        if word.len() < 3 {
            continue;
        }
        // or long ones, which tend to be more obscure and difficult to spell
        if word.len() > 7 {
            continue;
        }
        // no names
        if word.chars().nth(0).unwrap().is_uppercase() {
            continue;
        }
        // no plurals
        if word.ends_with('s') {
            continue;
        }
        wordlist.push(word);
    }
    println!("{} Words in the list", wordlist.len());

    let ri = RandIter::new(wordlist);

    let mut i = 0;

    for w in ri {
        print!("{} ", w);
        i += 1;
        if i > 3 {
            break;
        }
    }
    println!("");
}
